import { Injectable } from '@angular/core';
import { CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { NzNotificationService } from 'ng-zorro-antd';

import { UserService } from '~/shared/shared.module';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivateChild {

  guests = [
    '/login'
  ];

  constructor(private router: Router, private user: UserService, private notification: NzNotificationService) { }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.guests.find(guest => state.url.startsWith(guest))) {
      if (localStorage.getItem("access_token") == null)
        return true
      else {
        this.router.navigateByUrl('/page/application/application-list');
        return false
      }
    }

    return this.user.hasLogin().catch(() => {
      this.notification.error('Alert', 'User has not logged in！');
      this.router.navigate(['/login']);
      return Promise.resolve(false);
    });
  }
}
