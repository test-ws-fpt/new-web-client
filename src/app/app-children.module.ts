import { NgModule } from '@angular/core';

import { ExceptionModule } from './exception/exception.module';
import { LoginModule } from './login/login.module';
import { PageModule } from './page/page.module';
import {IndexModule} from './index/index.module'
@NgModule({
  imports: [
    ExceptionModule,
    LoginModule,
    IndexModule,
    PageModule
  ]
})
export class AppChildrenModule { }
