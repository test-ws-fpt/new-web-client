import { NgModule } from '@angular/core';
import { ApplicationModule } from './application/application.module';

@NgModule({
  imports: [ApplicationModule]
})
export class PageChildrenModule { }
