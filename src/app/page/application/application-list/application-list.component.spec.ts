import { async, ComponentFixture, fakeAsync, TestBed } from "@angular/core/testing"
import { FormsModule,ReactiveFormsModule, FormGroup, Validators  } from "@angular/forms";
import { BrowserModule, By } from "@angular/platform-browser";
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { UserService } from "~/shared/shared.module";
import {PageComponent} from "~/shared/components/page/page.component"
import { RouterModule, Routes } from '@angular/router';
import {LayoutComponent} from "~/shared/components/layout/layout.component"
import { AuthGuard } from "~/auth.guard";
import { APP_BASE_HREF } from "@angular/common";
import { ApplicationListComponent } from "./application-list.component";
import { ApplicationListService } from "~/shared/services/application-list.service";
describe('ApplicationListComponent', () => {
    let appList: ApplicationListComponent;
    let fixture: ComponentFixture<ApplicationListComponent>;
    let userServiceStub: Partial<UserService>;
    let appListServiceStub: Partial<ApplicationListService>;

    const routes: Routes = [
        {
          path: '',
          loadChildren: '~/app-children.module#AppChildrenModule',
          canActivateChild: [AuthGuard]
        },

      ];
    
    beforeEach(async(() => {
        userServiceStub = {
            // user: { name: 'Test User' },
        };
        const userService = jasmine.createSpyObj('UserService', ['auth']);
        const appListService = jasmine.createSpyObj('ApplicationListService', ['getApplications']);
        TestBed.configureTestingModule({
            imports: [
                ReactiveFormsModule,
                BrowserModule,
                NgZorroAntdModule,
                RouterModule.forRoot(routes),
                FormsModule,
            ],
            declarations: [
                LayoutComponent,
                ApplicationListComponent,
                PageComponent,
            ],
            providers: [
                {
                    provide: UserService,
                    useValue: userService
                },
                {
                    provide: ApplicationListService,
                    useValue: appListService
                },
                {provide: APP_BASE_HREF, useValue: '/'},
                LayoutComponent
            ]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(ApplicationListComponent)
            appList = fixture.componentInstance
            fixture.detectChanges();
        })
    }));
    it('should create', () => {
        expect(appList).toBeTruthy();
    });
})
