import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApplicationListComponent } from './application-list.component';

const routes: Routes = [
  {
    path: 'application-list',
    component: ApplicationListComponent
  },
  {
    path: '',
    redirectTo: 'application-list'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationListRoutingModule { }
