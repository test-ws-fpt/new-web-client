import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { Application } from '~/model/common';
import { ApplicationListService } from '~/shared/services/application-list.service';
import { UserService } from '~/shared/shared.module';

@Component({
  selector: 'app-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.less']
})
export class ApplicationListComponent implements OnInit {
  constructor(private user: UserService, private router: Router,
    private message: NzMessageService, private applicationListService: ApplicationListService) { }
  total = 0;
  loading = true;
  listApplications: Application[] = [];
  listCurrentApplications: Application[] = [];
  ngOnInit() {
    this.getListApplications();
  }

  // get list applications 
  getListApplications(): void {
    this.applicationListService.getApplications().subscribe(data => {
      this.loading = false;
      //set no column values
      data.applications.forEach(element => {
        element.no = 0;
      });
      let data_length = data.applications.length;
      for (let i = 1; i < data_length; i++) {
        data.applications[i - 1].no += i;
      }
      data.applications[data_length - 1].no += data_length;
      this.total = data_length;
      this.listApplications = data.applications;
    });
  }

  onCurrentPageDataChange($event: Application[]) {
    this.listCurrentApplications = $event
  }
}
