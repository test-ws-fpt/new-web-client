import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationDetailsComponent } from './application-details/application-details.component';
import { ApplicationListComponent } from './application-list/application-list.component';

import { ApplicationComponent } from './application.component';

const routes: Routes = [
  {
    path: 'application',
    component: ApplicationComponent,
    loadChildren: '~/page/application/application-children.module#ApplicationChildrenModule'
  },
  {
    path: '',
    redirectTo: 'application'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationRoutingModule { }
