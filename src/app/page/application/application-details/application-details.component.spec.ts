import { async, ComponentFixture, fakeAsync, TestBed } from "@angular/core/testing"
import { FormsModule,ReactiveFormsModule, FormGroup, Validators  } from "@angular/forms";
import { BrowserModule, By } from "@angular/platform-browser";
import { NgZorroAntdModule } from 'ng-zorro-antd';
import {PageComponent} from "~/shared/components/page/page.component"
import { RouterModule, Routes } from '@angular/router';
import {LayoutComponent} from "~/shared/components/layout/layout.component"
import { AuthGuard } from "~/auth.guard";
import { APP_BASE_HREF } from "@angular/common";
import { ApplicationDetailsComponent } from "./application-details.component";
import { ApplicationDetailsService } from "~/shared/services/application-details.service";
describe('ApplicationDetailsComponent', () => {
    let appDetail: ApplicationDetailsComponent;
    let fixture: ComponentFixture<ApplicationDetailsComponent>;
    let userServiceStub: Partial<ApplicationDetailsService>;

    const routes: Routes = [
        {
          path: '',
          loadChildren: '~/app-children.module#AppChildrenModule',
          canActivateChild: [AuthGuard]
        },

      ];
    
    beforeEach(async(() => {
        userServiceStub = {
            // user: { name: 'Test User' },
        };
        const appDetailsService = jasmine.createSpyObj('ApplicationDetailsService', ['getAllDetails']);
        TestBed.configureTestingModule({
            imports: [
                ReactiveFormsModule,
                BrowserModule,
                NgZorroAntdModule,
                RouterModule.forRoot(routes),
                FormsModule,
            ],
            declarations: [
                LayoutComponent,
                ApplicationDetailsComponent,
                PageComponent,
            ],
            providers: [
                {
                    provide: ApplicationDetailsService,
                    useValue: appDetailsService
                },
                {provide: APP_BASE_HREF, useValue: '/'},
                LayoutComponent
            ]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(ApplicationDetailsComponent)
            appDetail = fixture.componentInstance
            fixture.detectChanges();
        })
    }));
    it('should create', () => {
        expect(appDetail).toBeTruthy();
    });

})
