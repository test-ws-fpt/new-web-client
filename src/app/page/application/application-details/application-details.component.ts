import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApplicationDetail, Budget, Content, Member, Stage } from '~/model/common';
import { ApplicationDetailsService } from '~/shared/services/application-details.service';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-application-details',
  templateUrl: './application-details.component.html',
  styleUrls: ['./application-details.component.less']
})
export class ApplicationDetailsComponent implements OnInit {
  title = "";
  loading = true;
  totalStage = 0; totalBudget = 0; totalMember = 0;
  application_detail: ApplicationDetail;
  stages: Stage[] = [];
  member_roles: Member[] = [];
  budgets: Budget[] = [];
  sumBudgets = 0; keys: string[]
  stageContent = {}
  listCurrentMembers: Member[] = [];
  listCurrentStages: Stage[] = [];
  listCurrentBudgets: Budget[] = [];
  role_id = ""
  display = false; has_budget = false;
  color = "gray"
  constructor(private applicationDetailsService: ApplicationDetailsService,
    private message: NzMessageService, private router: Router) { }

  ngOnInit() {
    this.getApplicationDetails();
  }

  getApplicationDetails(): void {
    this.loading = true;
    this.applicationDetailsService.getAllDetails().subscribe(data => {
      this.loading = false;
      // get role id of current user
      this.role_id = localStorage.getItem("role_id")
      this.application_detail = data;
      this.title = this.application_detail.title;
      // check status for displaying buttons
      if (this.application_detail.status == "Pending For Final Approval" && this.role_id == "1")
        this.display = true
      if (this.application_detail.status != "Waiting For Final Update" &&
        this.application_detail.status != "Approved" && this.application_detail.status != "Rejected"
        && this.application_detail.status != "Pending For Final Approval")
        this.display = true
      if(this.application_detail.status == "Approved")
        this.color = "blue"
      if(this.application_detail.status == "Rejected")
        this.color = "red"
      // check if scale of the event is set
      if (!this.application_detail.scale)
        this.application_detail.scale = 0
      // check if event runner inserts required information
      if (this.application_detail.stages) {
        this.stages = this.application_detail.stages;
        this.totalStage = this.stages.length
        for (let i = 0; i < this.stages.length; i++) {
          this.stageContent[this.stages[i].id.toString()] = this.stages[i].contents
        }
        this.keys = Object.keys(this.stageContent)
      }
      if (this.application_detail.members) {
        this.member_roles = this.application_detail.members;

        this.member_roles.forEach((role) => {
          role.event_role_name = this.applicationDetailsService.convertIdToRoleName(role.event_role_id)
        })
        this.totalMember = this.member_roles.length
      }
      if (this.application_detail.budgets) {
        this.budgets = this.application_detail.budgets;
        this.budgets.forEach((budget) => {
          this.sumBudgets += budget.total_price
        })
        this.totalBudget = this.budgets.length
      }
      if (this.sumBudgets > 0)
        this.has_budget = true
    });
  }

  setApprovalStatus() {
    var application_id = localStorage.getItem("application_id")
    // if user is department president
    if (this.role_id == '1')
      this.applicationDetailsService.setStatusApplication(application_id, 4).subscribe(
        (data) => {
          setTimeout(() => {
            this.message.info('Final Approval Successfully!');
            this.router.navigateByUrl('/page/application/application-list');
          }, 1000);
        },
        (error: Response) => {
          if (!navigator.onLine) {
            this.message.create('error', `something wrong happened. Please try again!`);
          }
          else
            this.message.create('error', `Application is not updated`);
        });
    else {
      // not department president, preliminary approve the application
      this.applicationDetailsService.setStatusApplication(application_id, 2).subscribe(
        (data: any) => {
          this.application_detail.approved_by = localStorage.getItem("user-name")
          setTimeout(() => {
            this.message.info('Preliminary Approve Successfully!');
            this.router.navigateByUrl('/page/application/application-list');
          }, 1000);
        },
        (error: Response) => {
          if (!navigator.onLine) {
            this.message.create('error', `something wrong happened. Please try again!`);
          }
          else
            this.message.create('error', `Application is not updated`);
        });
    }
  }

  setRejectStatus() {
    var application_id = localStorage.getItem("application_id")
    this.applicationDetailsService.setStatusApplication(application_id, 5).subscribe(
      (data) => {
        setTimeout(() => {
          this.message.info('Reject Successfully！');
          this.router.navigateByUrl('/page/application/application-list');
        }, 1000);
      },
      (error: Response) => {
        if (!navigator.onLine) {
          this.message.create('error', `something wrong happened. Please try again!`);
        }
        else
          this.message.create('error', `Application is not updated`);
      });
  }

  // event for paging contents in each stage
  onCurrentPageContentsChange(id: string, $event: Content[]) {
    for (let i = 0; i < this.keys.length; i++) {
      // check if contents belongs to this stage id, set event for contents 
      if (this.keys[i].match(id)) {
        this.stageContent[id] = $event
      }
    }
  }

  onCurrentPageStagesChange($event: Stage[]) {
    this.listCurrentStages = $event
  }

  onCurrentPageMembersChange($event: Member[]) {
    this.listCurrentMembers = $event
  }

  onCurrentPageBudgetsChange($event: Budget[]) {
    this.listCurrentBudgets = $event
  }
}

