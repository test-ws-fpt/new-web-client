import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { SharedModule } from '~/shared/shared.module';
import { ApplicationDetailsRoutingModule } from './application-details-routing.module';
import { ApplicationDetailsComponent } from './application-details.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    NgZorroAntdModule,
    SharedModule,
    FormsModule,
    ApplicationDetailsRoutingModule
  ],
  declarations: [ApplicationDetailsComponent]
})
export class ApplicationDetailsModule { }
