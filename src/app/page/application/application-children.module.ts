import { NgModule } from '@angular/core';
import { ApplicationListModule } from './application-list/application-list.module';
import { ApplicationDetailsModule } from './application-details/application-details.module';

@NgModule({
  imports: [ApplicationListModule, ApplicationDetailsModule]
})
export class ApplicationChildrenModule { }
