import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { JwtService } from '~/shared/modules/jwt/jwt.service';

import { UserService } from '~/shared/shared.module';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  validateForm: FormGroup;
  submitting = false;

  region = {
    sider: false,
    header: false,
    footer: false
  };

  get username() { return this.validateForm.get('userName'); }
  get password() { return this.validateForm.get('password'); }

  submitForm() {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    if (!this.validateForm.valid) {
      return false;
    }
    this.submitting = true;

    this.user.login(this.username.value, this.password.value).subscribe(
      (data: any) => {
        // get access token for user
        this.jwt.token = data.access_token;
        setTimeout(() => {
          this.user.current = {
            name: localStorage.getItem("user-name")
          };
          this.message.info('Login Successfully！');
          this.submitting = false;
          this.router.navigateByUrl('/page/application/application-list');
        }, 1000);
      },
      (error: Response) => {
        // if internet connection is interrupted
        if (!navigator.onLine) {
          this.message.create('error', `something wrong happened. Please try again!`);
        }
        else
          this.message.create('error', `wrong username or password`);
        this.submitting = false;
      });
  }

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private jwt: JwtService,
    private message: NzMessageService,
    private user: UserService
  ) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      userName: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(50)]],
      password: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(50)]],
    });
  }

}
