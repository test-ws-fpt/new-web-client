import { DebugElement } from "@angular/core";
import { async, ComponentFixture, fakeAsync, TestBed } from "@angular/core/testing"
import { FormsModule,ReactiveFormsModule, FormGroup, Validators  } from "@angular/forms";
import { BrowserModule, By } from "@angular/platform-browser";
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { UserService } from "~/shared/shared.module";
import { LoginComponent } from "./login.component"
import {PageComponent} from "~/shared/components/page/page.component"
import { RouterModule, Routes } from '@angular/router';
import {LayoutComponent} from "~/shared/components/layout/layout.component"
import { AuthGuard } from "~/auth.guard";
import { APP_BASE_HREF } from "@angular/common";
describe('LoginComponent', () => {
    let login: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let de: DebugElement;
    let el: HTMLElement;
    let userServiceStub: Partial<UserService>;

    const routes: Routes = [
        {
          path: '',
          loadChildren: '~/app-children.module#AppChildrenModule',
          canActivateChild: [AuthGuard]
        },
      ];
    
    beforeEach(async(() => {
        userServiceStub = {
            // user: { name: 'Test User' },
        };
        // const testQuote = 'Test Quote';
        const userService = jasmine.createSpyObj('UserService', ['login']);
        // const getQuoteSpy = userService.getQuote.and.returnValue(of(testQuote));
        TestBed.configureTestingModule({
            imports: [
                ReactiveFormsModule,
                BrowserModule,
                NgZorroAntdModule,
                RouterModule.forRoot(routes),
                FormsModule,
            ],
            declarations: [
                LayoutComponent,
                LoginComponent,
                PageComponent,
            ],
            providers: [
                {
                    provide: UserService,
                    useValue: userService
                },
                {provide: APP_BASE_HREF, useValue: '/'},
                LayoutComponent
            ]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(LoginComponent)
            login = fixture.componentInstance
            de = fixture.debugElement.query(By.css("form"))
            el = de.nativeElement
            fixture.detectChanges();
        })
    }));
    it('should create', () => {
        expect(login).toBeTruthy();
    });

    // test action event
    it('Should call the submitForm method', () => {
        fakeAsync(() => {
            fixture.detectChanges();
            spyOn(login, 'submitForm');
            el = fixture.debugElement.query(By.css('Login')).nativeElement;
            el.click();
            expect(login.submitForm).toHaveBeenCalledTimes(0);
        })

    });

    // test form invalid input
    it('Form should be invalid', async(() => {
        login.validateForm.controls['userName'].setValue('');
        login.validateForm.controls['password'].setValue('');
        expect(login.validateForm.valid).toBeFalsy();
    }));

    // test form with correct input
    it('Form should be valid', async(() => {
        login.validateForm.controls['userName'].setValue('test_department_user');
        login.validateForm.controls['password'].setValue('123456');
        expect(login.validateForm.valid).toBeTruthy();
    }));

})
