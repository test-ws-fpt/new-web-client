import { NgModule } from '@angular/core';
import { LayoutModule } from './layout/layout.module';
import { NavbarModule } from './navbar/navbar.module';
import { PageModule } from './page/page.module';
import { SettingDrawerModule } from './setting-drawer/setting-drawer.module';
import { ToolbarModule } from './toolbar/toolbar.module';

@NgModule({
  exports: [
    LayoutModule,
    NavbarModule,
    PageModule,
    SettingDrawerModule,
    ToolbarModule
  ]
})
export class ComponentsModule { }
