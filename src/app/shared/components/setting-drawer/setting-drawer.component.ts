import { Component, Input, Output, EventEmitter } from '@angular/core';

interface SettingInterface {
  theme: string;
  color: string;
  mode: string;
  fixedWidth: boolean;
  colorweak: boolean;
}

@Component({
  selector: 'setting-drawer',
  templateUrl: './setting-drawer.component.html',
  styleUrls: ['./setting-drawer.component.less']
})
export class SettingDrawerComponent {

  @Output() settingChange = new EventEmitter();

  @Input() innerClass: object = {};
  @Input()
  get setting(): SettingInterface {
    return this.options;
  }
  set setting(val: SettingInterface) {
    this.options = val;
    this.settingChange.emit(this.options);
  }

  isCollapsed = true;
  options = {
    theme: 'dark',
    color: 'daybreak',
    mode: 'side',
    fixedWidth: false,
    colorweak: false
  };

  themes = [
    {
      key: 'dark',
      image: '/assets/images/theme-dark.svg',
      title: 'Dark'
    },
    {
      key: 'light',
      image: '/assets/images/theme-light.svg',
      title: 'Light'
    }
  ];

  others = [
    {
      key: 'colorweak',
      title: 'Weak Color'
    }
  ];

}
