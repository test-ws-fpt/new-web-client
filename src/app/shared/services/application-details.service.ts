import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { JwtService } from '../modules/jwt/jwt.service';
import { ApplicationDetail } from '~/model/common';
import { ActivatedRoute } from '@angular/router';
import { catchError } from 'rxjs/operators';
@Injectable({ providedIn: 'root' })
export class ApplicationDetailsService {
    id = "";
    baseURL: string = "https://ems-api.simplework.dev/application/applications/";
    statusURL: string = "https://ems-api.simplework.dev/application/applications/status";
    errorData!: {};
    constructor(private http: HttpClient, private router: ActivatedRoute, private jwt: JwtService) { }

    getAllDetails(): Observable<ApplicationDetail> {
        const auth_token = this.jwt.token
        // get id of the application from router link
        this.id = this.router.snapshot.queryParamMap.get('id');
        localStorage.setItem("application_id", this.id);
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + auth_token
        })
        return this.http.get<ApplicationDetail>(this.baseURL + this.id, { headers: headers }).pipe(
            catchError(this.handleGetDetailError)
        );
    }

    // set status of the application 
    setStatusApplication(applicationId: string, statusId: number) {
        const auth_token = this.jwt.token
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + auth_token
        })
        var app_id = +applicationId // convert string to number
        var postData = { application_id: app_id, status: statusId }
        return this.http.post(this.statusURL, postData, { 'headers': headers }).pipe(
            catchError(this.handleSetStatusError)
        );
    }

    private handleGetDetailError(error: HttpErrorResponse) {
        if (!navigator.onLine) {
            console.error('Internet is not connected');
        }
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
        }
        this.errorData = {
            errorTitle: 'Oops! Get detail application request failed',
            errorDesc: 'Something bad happened. Please try again later.'
        };
        return throwError(this.errorData);
    }

    private handleSetStatusError(error: HttpErrorResponse) {
        if (!navigator.onLine) {
            console.error('Internet is not connected');
        }
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
        }
        this.errorData = {
            errorTitle: 'Oops! Set application status request failed',
            errorDesc: 'Something bad happened. Please try again later.'
        };
        return throwError(this.errorData);
    }

    // get role name from role id
    convertIdToRoleName(id: number) {
        switch (id) {
            case 1: {
                return "Event Manager"
            }
            case 2: {
                return "Human Resource Leader"
            }
            case 3: {
                return "Content Leader"
            }
            case 4: {
                return "Finance Leader"
            }
            case 5: {
                return "Media Leader"
            }
            case 6: {
                return "Logistic Leader"
            }
            case 7: {
                return "External Parties Leader"
            }
            case 8: {
                return "Other Leader"
            }
            default: {
                return "Not Leader"
            }
        }
    }
}