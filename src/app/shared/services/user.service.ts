import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { JwtService } from '../modules/jwt/jwt.service';
@Injectable({
  providedIn: 'root'
})

export class UserService {
  current = {
    name: "Guest"
  }

  baseURL: string = "https://ems-api.simplework.dev/auth/login";
  userURL: string = "https://ems-api.simplework.dev/auth/who-am-i";
  errorData!: {}; data = [];
  constructor(private http: HttpClient, private jwt: JwtService, private router: Router) { }
  // login with role group 1 and correct username and password
  login(username: string, password: string) {
    const headers = { 'content-type': 'application/json' }
    var postData = { username: username, password: password, role_group: 1 }
    return this.http.post(this.baseURL, postData, { 'headers': headers })
      .pipe(
        catchError(this.handleError)
      );
  }

  // check permission of user to access list applications
  auth() {
    const auth_token = this.jwt.token
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + auth_token
    })
    const promise = new Promise((resolve, reject) => {
      this.http
        .get(this.userURL, { 'headers': headers })
        .toPromise()
        .then((res: any) => {
          localStorage.setItem("user-name", res.name),
          localStorage.setItem("role_id", res.role_id)
          this.current.name = localStorage.getItem("user-name")
          resolve(true);
        },
          err => {
            // Error
            reject(err);
          }
        );
    });
    return promise;
  }

  private handleError(error: HttpErrorResponse) {
    if (!navigator.onLine) {
      console.error('Internet is not connected');
    }
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    this.errorData = {
      errorTitle: 'Oops! Login request failed',
      errorDesc: 'Something bad happened. Please try again later.'
    };
    return throwError(this.errorData);
  }

  notice = {
    spinning: false,
    clear: function (type) {
      this.data = this.data.filter(x => x.type != type);
    },
    visibleChange: function (status) {
      if (status) {
        this.spinning = true;
        setTimeout(() => this.spinning = false, 1000);
      }
    }
  };

  hasLogin(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.auth()
        .then((data) => {
          resolve(true);
        })
        .catch((err) => {
          reject(false);
        });
    });
  }

  logout(): Promise<boolean> {
    this.current = null;
    // remove current access token
    localStorage.removeItem("access_token")
    return this.router.navigateByUrl('/login');
  }

}
