import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

interface Login {
    success: boolean
}
interface User {
    user_id: number,
    organization_id: number,
    name: string,
    role_id: number
}
const testLoginUrl = "https://ems-api.simplework.dev/auth/login";
const testAuthUrl = "https://ems-api.simplework.dev/auth/who-am-i";

describe('UserService testing', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
        });
        // Inject the http service and test controller for each test
        httpClient = TestBed.get(HttpClient);
        httpTestingController = TestBed.get(HttpTestingController);
    });
    afterEach(() => {
        // After every test, assert that there are no more pending requests.
        httpTestingController.verify();
    });

    it('can test HttpClient.post', () => {
        const testData: Login = { success: true };
        var postData = { username: "test_department_user", password: "123456", role_group: 1 }
        httpClient.post<Login>(testLoginUrl, postData)
            .subscribe(data =>
                expect(data).toEqual(testData)
            );

        // The following `expectOne()` will match the request's URL.
        const req = httpTestingController.expectOne('https://ems-api.simplework.dev/auth/login');

        expect(req.request.method).toEqual('POST');
        // Respond with mock data, causing Observable to resolve.
        req.flush(testData);
        // Finally, assert that there are no outstanding requests.
        httpTestingController.verify();
    });

});


