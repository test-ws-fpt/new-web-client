import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { JwtService } from '../modules/jwt/jwt.service';
import { Application } from '~/model/common';
import { catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ApplicationListService {
  baseURL: string = "https://ems-api.simplework.dev/application/applications";
  errorData!: {};
  constructor(private http: HttpClient, private jwt: JwtService) {
  }

  // get list applications sent to department
  getApplications(): Observable<{ applications: Application[] }> {
    const auth_token = this.jwt.token
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + auth_token
    })
    return this.http.get<{ applications: Application[] }>(this.baseURL, { headers: headers }).pipe(
      catchError(this.handleGetApplicationsError)
    );
  }

  private handleGetApplicationsError(error: HttpErrorResponse) {
    if (!navigator.onLine) {
        console.error('Internet is not connected');
    }
    if (error.error instanceof ErrorEvent) {
        console.error('An error occurred:', error.error.message);
    } else {
        console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    this.errorData = {
        errorTitle: 'Oops! Get list applications request failed',
        errorDesc: 'Something bad happened. Please try again later.'
    };
    return throwError(this.errorData);
}
}