export interface Application {
    id: number;
    no: number;
    title: string;
    status: string;
}

export interface ApplicationDetail {
    title: string;
    scale: number;
    description: string;
    members: Member[];
    stages: Stage[];
    budgets: Budget[];
    created_by: string;
    approved_by: string;
    start_date: Date;
    status: string;
    end_date: Date;
}

export interface Member {
    memberId: number;
    name: string;
    event_role_id: number;
    event_role_name: string;
}

export interface Stage {
    id: number;
    location: string;
    name: string;
    description: string;
    contents: Content[];
    start_date: Date;
    end_date: Date;
}

export interface Content {
    id: number;
    name: string;
    description: string;
    leader_id: number;
    leader_name: string;
    start_date: Date;
    end_date: Date;
}

export interface Budget {
    description: string;
    total_price: number;
}