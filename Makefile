NAME=web-client
IMAGE_NAME=docker.vctek.net/ems/$(NAME)
IMAGE_VERSION=1.0.1


.PHONY: build
build:
	npm run build

	
.PHONY: docker
docker:
	docker build -t $(IMAGE_NAME):$(IMAGE_VERSION) .
	docker tag $(IMAGE_NAME):$(IMAGE_VERSION) $(IMAGE_NAME):latest
	docker push $(IMAGE_NAME):$(IMAGE_VERSION)
	docker push $(IMAGE_NAME):latest